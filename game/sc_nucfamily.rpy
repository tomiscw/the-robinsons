################################################################################
## This file is licensed to you under the MPL 2.0 license.
## See the LICENSE file in the project root for more information.

label nucfamily:

    # EXT.-ROBINSON HOME-NIGHT
    # Lights burn in the windows.

    john "Maureen?"
    john "Maureen, you there?"

    # INT.-ROBINSON HOME-DINING ROOM-NIGHT
    # John ENTERS to find the remains of an elegant dinner on the table,
    # candles never lit, food never eaten.
    # John crosses to a small scale model sitting on the table. Around it
    # hangs another gold plated 1st prize star. He smiles.

    maur "He won first prize again. He practically brought down the entire school."
    maur "But he won first prize."
    $ delay()
    john "..."
    john "For a non-working prototype for his time machine? Sharp stuff for a kid."
    $ delay()
    maur "..."
    john "I'm sorry about dinner. I had to work late. The new pilot-"
    maur "What you had to do was prioritize your family over the mission."
    john "Honey, the mission about the our family. So we can stay together."
    maur "The perfect nuclear family. The most stable social unit, ideally suited for isolation of a long journey into space. It's all PR, John. You're never home."
    maur "Judy is becoming a ghost just like."
    maur "Penny can't see past her own navel and Will has to black out his school,"
    maur "just to get his father attention."
    maur "There is no nuclear family here, John. Don't you see it?"

    # John stares at his wife. Then he reaches forward, touches her hair.

    john "I know, Maureen. I'm scared too."

    # A beat. Then she moves to him. And he holds her close.

    # INT.-ROBINSON HOME-WILL'S ROOM-NIGHT
	# John stands in the yellow wedge of hall light. He touches something
    # hanging around his own neck. A pair of metal dog tags.

	# WIDER
	# Will lies in his bed, eyes closed. Asleep. A beat. John turns, heading
    # out into the hall.

	# EXT.-ROBINSON HOME-NIGHT
	# The bedroom lights go out, a normal home against the backdrop of the
    # launch dome towering in the distance.

	# INT.-SPACE COMMAND-LAUNCH DOME-NIGHT
	# The Jupiter one stands amidst loading gantries, final supplies rolling
    # into the ship on automated conveyer belts and lifts.

    jump primdir

label primdir:

    tech "Anything else?"

    ltech "I'm showing a late shipment from the Medical Division."
    ltech "Smith's authorization. Here it is now."

    # HOLD on a canister that reads: BIOLOGICAL MATERIALS: Do Not Open, as it
    # passes on the loading belt. FOLLOW THE DRUM as it winds along the
    # conveyer, up a gantry towards the Jupiter One. PUSH IN...

    # INT. -CARGO DRUM
    # Smith sits crouched inside, cleaning his nails.

    # INT.-JUPITER ONE-LOWER DECKS
    # A giant robot stands attached to its service bay. Still. Silent.

    # A SERVICE DRAWER slides open and a shadowy infiltrator emerges, his
    # face visible in the dim service light. Smith.

    # Smith approaches the docked Robot, places a small keypad device on the
    # Robot's dormant CPU panel. Smith activates the device.

    smith "You'll forgive me if I forgo a kiss, my sleeping behemoth."

    # Smith hits a switch on the panel and the Robot's system indicators
    # light into life. Smith types several keys on the keypad.

    smith "But the time has come to awake."

    robot "Robot is online. Reviewing primary directives."
    robot "One: preserve Robinson family. Two: maintain ship systems-"
    smith "Spare me the chatter."

    # Smith taps the keypad. The Robot is silenced. Smith types again.

    smith "What a noble charges my steely centurian. Sadly, I fear you have more dire deeds in store for you."

    # Smith completes his re-programming. Hits a switch.

    robot "Robot is online. Reviewing primary directives."
    robot "Two hours into mission destroy Robinson family. Destroy all systems."

    # Smith smiles. Removes the re-programming module.

    smith "Now that's more like it. Farewell my tin-plated pal."
    smith "Give my regards to your oblivion."

    # Smith climbs into a chute marked WASTE DISPOSAL.

    # INT.-WASTE DISPOSAL CHUTE

	# Smith begins crawling down the chute. His tiny reprogramming module
	# BEEPS. Smith activates a tiny, built in com-link.

	# From the mic, the familiar visage of the Businessman is projected into
	# the crawlspace over Smith's face.

    smith "I told you to never call me here."
    bman "I see you have completed your mission on schedule."
    bman "Good work, Doctor. And goodbye."

    # The Businessman smiles. SMITH-CLOSE. Puzzled.

	# Suddenly the tiny module in his hand OVERLOADS, the flesh on his palm
	# smoking as Smith is engulfed in an electrical charge. He goes out, his
	# body perfectly still.

    jump liftoff
