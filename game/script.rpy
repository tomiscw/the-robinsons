﻿################################################################################
## This file is licensed to you under the MPL 2.0 license.
## See the LICENSE file in the project root for more information.

label start:

    # INSERT CORPORATE LOGO.-A Icy Blaze bottle hurling towards the stars.

    "Imagine an end to world hunger. What ample food and clean drinking were the bightlight of all our planet's children?"

    "This man, professor John Robinson, the inventor of the faster than light hyerdrive can make the timeless dream a reality."

    "John Robinson and his family have been extensively traing to take a ten year journey across the galaxy in the world's most advanced spaceraft, the Jupiter."

    "From a distant world, the Robinsons, will bring back a miracle..."

    "Dimondium can turn even worthall sand to fertile soil. Earth will be a garden. What kind of future can our children look foward to do?"

    # WILL ROBINSON (10) hides in a small space, watching the commercial
    # on a jury-rigged, palm-sized computer. He mimics the Naylrator.
    will "Icy Blaze. Saving the world for our children."
    $ delay()
    will "Give me a break."

    # INT:-LIVING ROOM-AFTERNOON

    maur "Good evening, Principal Coppin. What did Will do this time?"
    # Basic twenty first century modern. MAUREEN ROBINSON stands, talking with a PRINCIPAL who in less than happy.
    prin "He hacked our main power grid to run his experiment. The school was in chaos. We didn't even have lights."
    # Maureen LAUGHS, then realizes the Principal has no idea her image is being distorted.
    # Maureen begins moving about the room, surreptitiously glancing behind couches, into cabinets.
    prin "This is no laughing matter, Mrs. Robinson. Will is terribly gifted. His little time machines, though pure fancy, are the products of a truly brilliant mind."
    # The Principal's body has become Twiggy's. Now that of an ape. Maureen pulls open a closet. Will sits inside. Grins.
    will "The changing shape of education."
    maur "No more monkey business."
    # Will shrugs, adjusts his deck. The Principal returns to normal.
    $ delay()
    prin "The boy is starved for attention. Was there no way his father could have attended the science fair?"

    jump themission

label themission:

    # INT.-SPACE COMMAND-MISSION CONTROL-DAY

    # A MAN stands before a large viewscreen facing the throng of PRESS.
    # He's a little uncomfortable with all the media attention.
    # This is PROFESSOR JOHN ROBINSON.
    john "Once we have landed on Alpha Prime our on board robot will begin construction of a hypergate."
    # ON SCREEN a hi-tech orbital gate is highlighted and expanded.
    john "By then, technicians here on Earth will have complated a companion hypergate in our planet's obit."
    # ON SCREEN an image of an orbital gate now under construction.
    john "Once both gates are complete, ships will be able to pass instantaneosly between them. The Jupiter can bring the Dimodium back to earth without the ten year delay of a return trip."
    rp "Why you can't just use the Juipter's hyperengine to zap straight to Alpha Prime?"
    john "As you know, hyperspace exists beneath normal spae. If you try to enter without a gate, "
    # ON SCREEN a graphic spaceship appears randomly in the corner of a turning
    # schematic of the galaxy.
    john "your exit vector is random. There's no telling where you'd come out."
    rp "Professor, how is Captain Daniels reovering from the flu? Will we he be able to pilot the mission?"
    # John glances to the doorway where a uniformed GENERAL stands.
    gen "Ladies and Gentleman, you came to look at the Jupiter One. Don't you think you've waited long enough?"
    # John hits a button and the room darkens. A giant monitor reveals...

    # INT.-LAUNCH DOME
    # A giant saucer sits connected to its launch pad by loading belts,
    # steaming fuel tubes and spindled gantries.
    rp "Professor, how does your family feel about leaving Earth behind?"
    john "They couldn't be more excited."

    jump goodbye

label goodbye:

    # EXT.-ROBINSON HOUSE-ESTABLISHING

    penny "This mission sucks!"

    # INT.-ROBINSON HOME HALLWAY-SUNSET
    maur "We will talk about this later, Penny."
    penny "I don't want to stay home for dinner. I want to see my friends for the last time."

    # She takes a beat, blinking away the tears. Lifts her wrist and turns on  a video camera watch. She begins down the hall.

    # VIDEO-POV. Penny's face in the monitor screen.
    penny "... On this even before she is torn from her frinds, kidnapped, hurled into deep space against her will, what thoughts fill mind of the young Space Captive."

    # INT.-WILL'S ROOM-SUNSET

    will "Will there be boys in Alpha Prime? What will I wear?"
    penny "When we wake up in ten years the video journals of Penny Robinson, Space Captive, will be devoured by millions. I'll be world famous. You on the other hand, will have been totally forgotten."

    # Penny shows the camera her arm, covered by strings of ribbons.

    penny "The Space Captive has decided to wear ribbons of support for fellow sufferers as she is dragged into deep space, green for ecological issues, and human rights."
    will "You'd probably if I desribed the secondarry infections loss of cirulcation can cause."
    # Penny seems about to speak, instead just smiles, goes to his bed and
    # pulls off the sheets, begins knotting them together.
    will "What are you grinning at?"
    penny "I just found an upside to this mission. The thought of jettisoning your body into deep space."
    # Penny knots the sheets into a rope, ties it to the bed-post. Will lifts several palm-sized gold-plated stars. Each reads: 1st prize.
    will "Dad says don't bring them. Like anything I do matters to him."
    penny "He never showed, huh?"
    will "Maybe if I had broken the time barrier he would have paid attention."
    penny "Don't let him get to you, kiddo. He just bets busy with work is all."
    # Penny tosses the rope out the window, prepares to climb.
    will "So is that a no to family dinner?"
    penny "Let's see, do I spend my last night on Earth watching our parents pretend not to be fighting again or get in ten years of making-out at the mall. You do the math."
    will "Mom's gonna go thermal."
    penny "What's she gonna do? Ground me?"

    jump mjrwest

label mjrwest:

    # INT.-SPACE COMMAND-CORRIDOR
    # John and the General walk the metal passageway.

    john "We're lucky the press didn't press on Daniel's conditiion."
    gen "I figured a chance to look at the ship would keep the dogs at bay."
    john "Ben, I'm worreid about jamming in a replacement pilot at the last second. I need someone who's moe then just spit and polish."
    gen "I've got your man. He just doesn't know it yet."

    # The General palms an access panel. A door hisses open to reveal...
    # INT.-SPACE COMMAND- CONFERENCE ROOM-CONTINUOUS
    # A figure stands staring silently out the window. Don West. He offers a
    # salute. By his eyes, West is clearly agitated.
    gen "At ease, Major."
    west "Sir, why was I pulled off active duty? I salvaged Jeb's and my crafts after I saved the Hypergate from another attack."
    west "I'll fight a court martial, General!"
    gen "Do you know Professor Robinson?"
    west "By reputation only."
    $ delay()
    west "Your father's battle strategies were required reading at the Academy."
    gen "How much do you know about the Jupiter Mission, Major?"
    west "The Jupiter is fully automated. The pilot flies the ship out of the solar system and lands on Alpha Prime. It's a baby sitting, Job."
    gen "Major, you're aware Earth's resources are drying up."
    west "Every schoolchild knows our recyling technolgies will save the enviroment. Sending a family across the galaxy is a publicity stunt to sell soda to people of all ages."
    gen "What I'm about to tell you is classified."
    $ delay()
    gen "Every schoolchild has been lied to. The recyling technolgies have failed. In four decades Earth will no longer able to support human life."
    gen "We spun the ad campaign to give the mission a friendly face. To keep people from panicking in the streets. We partnered with Icy Blaze because the 900 billion mission cost would have bankrupted the goverment."
    gen "The SinoJordanian Alliance is aware of the truth as well. They hope to reah Alpha Prime before we do. I guarantee you, will they wil not share the Dimondium with Western Demons like us, our crops will wither and we'll be left to die."
    $ delay()
    west "..."
    $ delay()
    extend "Captain Daniels doesn't have the flu, does he sir?"
    gen "Daniels was murdered in his aprtment last night. The flu is a mere cover story we fed the press."
    west "Dan was a good pilot. A good man."
    gen "Your rescue stunt bak on Mars was foolhardy. Explain yourself, Major."
    west "I had a friend in trouble."
    gen "You endangered a billion dollar spaceraft, disobayed a direct order, and all because of a friend?"
    west "Yes, sir. And I'd do it again."
    $ delay()
    extend "He would have done the same for me."
    john "He'll do."
    $ delay()
    west "Wait, wha!?"
    gen "Congratulations, Major, you're the new pilot of the Jupiter mission."

    jump drsmith

label drsmith:

    # Sun beats hard on a MAN who stands on a windswept sand dune, talking to a smartly dressed BUSINESSMAN.
    man "Perhaps a brief jaunt down memory lane is required. I was contracted to provide Danials, aparment code." # Smith
    man "My work is done."
    bman "They found a replacement pilot. The attack on the hypergate failed."
    man "How tragic. For you."
    bman "We require a more diret intervention on your part."
    $ delay()
    man "That'll cost on you. And I'm afraid my cost has become ..."
    man "astronomical."
    # (OVER) a KNOCK. The man reaches forward and presses a button. The  businessman, the desert disappear, only holographs.
    # Room lights.
    # Lights come up to reveal...
    # INT.-MISSION CONTROL-MEDICAL LABS
    # The man turns, revealing a face as clever an his eyes are evil. This is
    # DR. ZACHARY SMITH. He walks to the door, palms a panel, and the portal
    # hiss open. A TECHNICIAN ENTERS.
    smith "Holograph off."
    tech "Control hasn't received the results of your final pre-flight exams, Dr. Smith."
    # The Doctor moves to a circular overhead light board around which are
    # displayed the faces of the crew of the Jupiter craft.
    # Removes the  micro-files, hands them to the technician.
    smith "The Robinsons are all checked out at 100%. They're in perfect condition and ready to fly."
    tech "Sir, I can't give you the go ahead."
    # He smiles, so sweet you can barely see the malevolence behind it.
    smith "I don't need to."
    tech "Sir?"
    $ delay()
    smith "Wish them good luck for me."
    # Smith closes the door on the technician
    smith "Computer, reveal Jupiter blueprints."
    comp "Comfirmed."
    smith "Show waste disposal chutes."

    jump welaboard

label welaboard:

    # INT.-SPACE COMMAND-WALKING

    gen "Mission protocols are simple. Professor is in command unless you encouter a military emergency. In that case, Major West, you'll assume command."

    west "But the SinoJordanians could come back, General, and I'm a fighter pilot. I deserve to be back up there."
    gen "I agree."
    west "Then why won't you put me back up there?"
    gen "I need someone that can not only can care for the crew but I need someone who cares about this crew."
    west "But I barely even know these people."
    $ delay()
    west "No offense."
    john "None taken."
    west "My own family can't stand me."
    gen "10 years is a life time, Major. You'll have plenty of time to mingle."
    west "Yeah, but..."

    # INT.-JUPITER BRIDGE - CONTINUOUS

    west "Jeb. Jeb Walker is perfect for this mission-"
    john "Welcome aboard, Major."
    $ delay()
    west "..."
    extend " Wow!"
    # Hi-tech heaven. Two pilots, chairs face a giant windscreen. Don
    # walks to the consoles that stretch toward the back walls.
    john "Some of this technolgy might be new to you. I'll be happy to explain."
    west "Cold fusion drive."
    extend " Rambler-Krey Life Sciences stations."
    # Don strides to a pedestal in the center of the bridge. Flicks a switch
    # and a holographic Jupiter craft appears on the launch pad.
    west "Holograhpi navigational interface."
    # Don points to a row of man-sized glass cylinders on the back wall.
    extend " Cryosleep array with full monitoring!"
    # He approaches the blast doors that lead off the back of the bridge.
    west "Sick bay, remote ops, engineering and living quaters are below decks."
    west "Hyperengine should be in here."
    # Don hits a wall panel which opens to reveal an immense ENGINE ROOM.
    $ delay()
    west "I believe that's everything."
    john "Correct."
    john "If you have to baby sit, the Jupiter is not such a bad nursey. Wouldn't you agree, Major?"
    # Just then the ELEVATOR rises from the lower decks. On its circular
    # platform stands a DOCTOR in a lab coat. She walks to the General.
    judy "The cryosleep systems are running at 95%."
    gen "D. Smith approved of the specs-"
    judy "Dr. Smith is just the base physician. I'm repsonabile once the ship is in flight. These tubes have to keep the crew in suspended animation for ten years."
    judy "They will be perfect or this ship not launc. Is that clear?"
    gen "Absolutely, Doctor."
    john "Judy, I'd like you to meet Major West. He's taking Mike's place."
    judy "He's heavier than Mike. I'll have to re-calibrate."
    west "I'd be more than happy to discuss my dimensions perhaps over dinner?"
    judy "West - I've read about you. Something of a war hero, huh?"
    west "Well, yes, actually. You could say that."
    judy "Who was it that said \"those who can't think, fight\"?"
    $ delay()
    judy "I'd think it was me."
    judy "Anyway, it was nice meet you, Mr. West."
    # She heads toward the readouts. West turns to John.
    west "That's one cold fish I'd love to thaw."
    judy "I'm not going to make it home for dinner, Dad. Sorry."
    # West turns to John. Dad? Then John walks away.
    $ delay()
    west "It's gonna be a long flight."

    "The Robinson family have less then 24 hours until launch."
    # "You have time to make ends meet."

    menu:
        "Skip to launch?"
        "Primary Directives":
            jump nucfamily
        "Lift off":
            jump liftoff

label liftoff:

    # INT-JUPITER ONE  John and Maureen face will and Penny who stand now in
    # their freezing tubes. All  wear silver flight suits. Maureen moves
    # Penny's hair from in front of her face.

    penny "Don't, mom. Vogue says this will be in style in ten years."
    will "Will she wake up less annoying?"
    penny "Does he have to wake up at all?"
    maur "Alright, that's it. I'm turning this ship around."

    # Maureen kisses her children. John moves to will who reaches to shake at
    # the same moment John tries to hug him. A beat. Then John tries to a
    # shake as Will goes for the hug. A total miss. Will backs into his tube.

    maur "You get a C in paternal expression, Professor. But an A for effort."

    # John kisses his wife, already in her tube.

    maur "You always get an A in that."
    judy "Don't let me shatter this unbridled display of affection, but we're running behind."
    john "Major, she's all yours."

    noahf "Roger, Doctor, you are go to initate cryostasis."

    # Don walks to Judy's tube.

    west "One question, Doctor. Is there room in these tubes for two?"
    judy "There is barely enough room for you and your ego, Major."
    judy "Now, drive carefully."
    $ delay()
    judy "Initializing cryostasis tubes."
    west "..."

    # The tubes rotate closed around the Robinsons. Suddenly each crew member
    # is surrounded by a shimmering blue glow, brightening like a star and
    # then subsiding. All stand fixed in suspended animation.

    # Don stares at the family a beat. He goes to the main console. Straps
    # himself in.

    west "Mission Control, this is Jupiter One. The Robinsons are all tucked in. We are ready to fly."

    # INT.-SPACE COMMAND-MISSION CONTROL

    # Welcome to the future. Banks of monitors manned by gum-chewing, pink
    # haired technicians. A giant monitor shows the Jupiter on the launch
    # pad. The CONTROLLER (NOAH FREEMAN) pulls in to his console.

    noahf "Jupiter Two this is Mission Control. You are at T-minus two minutes and counting. We're opening the dome."

    # MONITOR-CLOSE. The dome begins to part.

    # EXT.-JUPITER ONE
    # Giant gantries retract. Fueling lines disengage.
    # INT.-JUPITER ONE

    west "External fuel pressure to zero. Powering main drive systems."
    noahf "Juiter One you are at 95%."
    west "Houston, main drive systems online. It's showtime."
    noahf "We have lift off."
    west "Requesting escape trajectory."
    noahf "Major, your escape vector is clear of all military and commerical traffic. Up is go on your command."
    west "Roger, Houston."
    $ delay()
    west "And the monkey flips the switch."

    # EXT. JUPITER ONE

    # The atomic power source fires a focused BLAST, shooting the Jupiter
    # through cloud and sky towards the black edge of space.

    noahf "Jupiter One, you're clear of Earth's atmophere."
    west "Jupiter One booster has disengaged. Proceeding towards Mercury."

    # EXT.-JUPITER TWO

    # Nuclear diodes at the ship's base spin into life, glowing with atomic
    # fire, the ship now heading towards Mercury and beyond.

    west "Houston, diverting all spacecraft control to the main computer."
    "Eight years of flight training."

    # HOLOGRAPH- CLOSE. The Jupiter Two is visible, backed by the planets of
    # our solar system.

    west "Navigational holographics online"
    "Fifty combat missions."

    # Don touches buttons and a highlight indicates the ship's trajectory
    # past mercury, around the Sun in a slingshot and into space beyond.

    west "Course confirmed for a slingshot exit of the solar system."
    $ delay()
    west "Just so I can take the family camper on an interstaller picnic."

    # Don walks to the freezing tubes. He zips up his silver flight suit.

    west "Ten world series. My newphew' high school and collage graduations. A decade's worth of Spots Illustrated."
    west "Ten years is a lifetime, Noah."

    # West climbs into his tube.
    # INT.-MISSION CONTROL

    noahf "Sleep well, old friend."

    # EXT.-JUPITER TWO
    # The ship flies on into the endless night.

    jump destroy

label destroy:

    # INT.-JUPITER TWO-LOWER DECKS

    # Smith scrambles out of the chute, stares at his hand, the impression of
    # the overloaded communicator seared into his palm.
    # He spins around disoriented. Stumbles to a closed view screen and hits
    # a stud. The blast shield opens. Space.

    # SMITH-CLOSE. Shock. No words.

    smith "Shit."

    robot "Robot is online. Implemtning prime ommand directives."
    robot "Destroy Robinson family. Destroy all systems."

    smith "Shit!"
    smith "No. Cease. Desist."

    # INT.-MISSION CONTROL-NIGHT

    # Dark. A single Techie (ANNIE) mans the watch, playing holo-games with a
    # stylus over her desk. SCREENS flicker into life.

    annie "What the!?"

    # INT.-JUPITER TWO-BRIDGE

    # Still. The bridge doors open and the ambient lights come on.

    # The Robot rolls onto the bridge, faces the cryosleep array. He extends
    # his arms, an electrical charge arcing between his claws.

    robot "Destroy the Robinson family. Destroy Juiter Two."
    smith "Mechanical moron. You'll kill us all!"

    # INT.-MISSION CONTROL

    # Fully lit. All the monitors are manned by sleepy Techies. Noah ENTERS,
    # taking a coffee from Annie without breaking stride. , ALARMS flash
    # everywhere.

    noahf "The mission is over. Wake them up."
    annie "I can't, sir. The computers are not responding."
    noahf "Get Hammond."

    # INT.JUPITER TWO

    # The Robot FIRES at the scrambling boy but misses, Will disappearing
    # down the gunny hatch leading to the lower decks.

    # DON leaps from his tube atop the Robot's back, trying to grab the
    # Robot's power source. The mechanical monster electrifies his shell,
    # sending the Major flying.

    # The Robot turns to Penny and Maureen. Extends his arms toward them.

    # Electricity arcing between his claws. The two are done for. Maureen
    # puts her hand over Penny's eyes.

    muar "Look away, baby."
    will "Robot. Stand down."
    robot "Command accepted."
    will "Return to your docking and power down."

    # The Robot turns and begins heading for the elevator. All watch in
    # amazement. Will grins, a familiar mischievous gleam in his eyes.

    will "If the family won't come to the science fair, bring the science fair to the family."
    penny "Show off."
    # An EXPLOSION rocks the ship.
    maur "Is that Doctor Smith?"
    # Penny stands over Smith who lays crumpled in the corner. Maureen grabs
    # an emergency medi-kit from a wall clamp, tosses it to Penny.
    maur "Pulse, respiration and bleeding-"
    penny "Basic triage, lock down and stabilize. I know the drill."
    # John is standing, helps Don to his feet.
    west "NEXT. Picnic, no robots."

    # INT.-MISSION CONTROL

    gen "Can we get life signs?"
    annie "Just barely."
    gen "Do it!"

    # INT.JUPITER TWO
    # Don almost buys it. Then he notices Smith's hand, grabs it.
    # BURN-CLOSE. The reprogramming module's distinctive, foreign signature
    # seared into his flesh.

    west "SinoJordanian technolgy. You're a damn spy."
    west "You did this!"

    # Don slams him against the bulkhead, begins dragging him towards the
    # airlock. The pilot hits a switch. The inner hatch opens.

    smith "Stop. what are you doing?"
    west "Throwing out the trash."
    maur "Help, somebody! Please."
    maur "The thawing engine is broken. I can't get Judy out."
    maur "She's dying!"
    smith "Touch me and the girl dies."
    west "I don't deal with dead men."
    smith "Kill me, kill the girl. How much is your revange worth, Major?"
    smith "I will, of course, need a word as an officer that you will let me live."
    west "Save her."

    # INT.-MISSION CONTROL

    annie "Jupiter Two is off course."
    noahf "Where are they headed?"
    annie "The sun."
    west "Mission Control, this Jupiter Two."
    noahf "We hear you."
    west "We're unable to re-route to Mercury. The sun's pull is too strong."
    west "I need options, Noah."
    noahf "Major West, we're unable to provide contingencies."
    $ delay()
    noahf "I'm sorry, Don."

    # INT.JUPITER TWO

    john "There has to be a way get through this."
    west "That's it."
    john "What's what?"
    west "If we can't go around the sun, we'll straight through it."
    john "Go into hyperspace without a gate you'll be thrown anywhere in the universe."
    west "Anywhere but here."

    # INT.-MISSION CONTROL

    annie "We can't keep a fix on her, sir. She's starting to disappear."
    # Noah stares at the screen.
    annie "It's gone."
    $ delay()
    noahf "Hammond."
    gen "Tell your family to start packing."

    # EXT.-DEEP SPACE

    # Quiet. Empty. Suddenly space distorts and, in a sudden flash, a ship
    # appears, hurling into the darkness. The Jupiter Two.

    # The Jupiter Two comes to rest at the edge of an alien, twin starred
    # solar system, near a giant crimson planet.



    return
