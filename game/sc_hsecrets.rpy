################################################################################
## This file is licensed to you under the MPL 2.0 license.
## See the LICENSE file in the project root for more information.

label hcity:

    # INT: LIVING ROOM - AFTERNOON
    "TV" "Jupiter 2 has been lost."
    ak "Daron, Felix, Rolan pack your bags."

    # EXT: PRIVATE AIRPORT
    # The three wear breathing masks to keep from
    # inhaling the toxics
    fw "Where are we headed?"
    ak "I can't discuss it here-"
    dc "Aki?"
    ak "The recyling planets."
    fw "What about them?"
    ak "I thought they decommissioned them."
    ry "He's right."
    ry "My readings tell me they were shut down decades ago."
    ak "Maybe Stacy would know."

    # EXT: ENDLESS DESERT ROAD
    dc "Wow. I didn't realize how much land we've lost."
    ak "Not many venture out this far. No point."
    $ delay()
    fw "You still haven't told us where we head us."
    ry "My GPS shows a deserted a toy a couple miles away."
    ak "That's because what I'm about to show you isn't on any map."

    # We reveal that that the Hu have created a hidden city in order
    # to remain neutral from the fight to Alpha Prime

    # EXT: HUT OUTSIDE HIGHLAND
    # A small hut located near far off the road
    sp "What are you doing here? You're supposed to be at Fairview."
    ak "We lost Jupiter 2."
    sp "..."
    sp "Who are them?"
    ak "I trust these two with my life."

    # INT: HOME IN HIGHLAND
    ak "I figured they would be safe here."
    sp "We can't keep them."
    ak "Why!?"
    sp "Highland isn't a lifeboat!"
    sp "We're already consuming enough power with the invisability cloak."
    # Daron raises his hand
    sp "What?"
    fw "Aki noticed the recyling plants were on again on our way here."
    ry "My readings make no sense."
    sp "Really?"
    ak "Yes."
    # Stacy reveals blueprints of the Proteus fleet ships
    sp "The Proteus fleet."
    dc "Proteus?"
    sp "They've been described as interstaller cruise ships,"
    sp "and meant to take thousands of passengers safely"
    sp "through the Alpha Prime gates."
    ak "Wait."
    ak "Where did you get this info?"
    sp "We were informed by varies Hu assigned at the plants."
    sp "And you guys need to get on those ships."

    dc "But what about Aki?"
    sp "What about him?"
    dc "Does he stay here while we go?"
    ak "Stacey?"
    sp "In Hu tradition, family means \"those who love and take care of each other\"."
    sp "And it seems like Aki cares a lot for all three of you."
    ak "When do we get our tickets?"
    "Daron and Felix" "Yay!"

    return

label learth:

    # EXT: Proteus space sport

    fw "They're a few more ships available."
    ak "Stacey designed these tickets to work with any."
    dc "I really wish Sam were here."
    fw "Me too."
    ak "The Welsh family are very wealthy. They would have found a way surviving."
    dc "You're right."

    # INT: Proteus 008 - Lobby

    dc "Not bad for recycled parts."
    ak "Daron."
    dc "What?"
    dc "It's true."
    comp "Depature for Alpha Prime in 5 hours."
    ak "We better unpack."

    #INT: Proteus 008 - Hallway

    $ hu_planet = "Jaslore 12"

    dc "What if we went to [hu_planet], instead?"
    # Aki pulls the two into a maintaince closet
    ak "We barely left earth and you don't even want to go to Alpha Prime?!"
    dc "We want to go Alpha Prime. But aren't you curious too?"
    ak "..."
    ak "Felix?"
    "Felix softly nodded."
    "Aki was reluctant to admit he was curious about [hu_planet] too."
    "He was a relative to Tao Page, an Earth Hu, not from anyone of [hu_planet]"
    ak "Sigh. We're not too far from our room."

    return
