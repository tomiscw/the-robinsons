################################################################################
## This file is licensed to you under the MPL 2.0 license.
## See the LICENSE file in the project root for more information.

## Characters ##################################################################
# Juipiter 2
define john = Character(_("John"))
define maur = Character(_("Maureen"))
define judy = Character(_("Judy"))
define west = Character(_("Maj. West"))
define will = Character(_("Will"))
define penny = Character(_("Penny"))
define smith = Character(_("Dr. Smith"))
define robot = Character(_("Robot"))
define comp = Character(_("Computer"))
# Proteus fleet
define jeb = Character(_("Jeb Walker")) # The lost ship from the movie
define fw = Character(_("Felix Wilcox")) # Descendant of Anthony Wilcox
define ry = Character(_("Rolan Yamamoto")) # Saveli's robot
define dc = Character(_("Daron Casey")) # Descendant of Zack Casey
define ak = Character(_("Aki Page")) # Descendant of Tao Page
# United Space Force
define gen = Character(_("General Hammond"))
define noahf = Character(_("Noah Freeman"))
define annie = Character(_("Annie"))
# Extras
define prin = Character(_("Principal Cartwright"))
define rp = Character(_("Reporter"))
define man = Character(_("Man"))
define bman = Character(_("Businessman"))
define tech = Character(_("Technician"))
define ltech = Character(_("Loading Technician"))
define sp = Character(_("Stacy Peters")) # Female Hu

## Constants ###################################################################
default music_dir = "music"
default bp_license = _("The binaries and source code of this program have been made available to you under the Mozilla Public License 2.0 (MPL).")

## Variables
define whatifarc = False

## Soundtrack ##################################################################
define theme_song = "[music_dir]/theme.ogg"
