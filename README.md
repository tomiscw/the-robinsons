# The Robinsons

The Robinsons is a re-imagining of Lost in Space as a visual novel. It was inspired of the 2018 Netflix series but uses the the 1998 movie and an late script draft for the source martial.  

The visual novel follows parts of the first and second acts of the film, while borrowing elements from an late draft of the screenplay. It expands on the conflict between United Global Space Force and SinoJordanian government, known as the Sedition in the movie, as well as expanding on the Proteus concept but makes them more similar to the Resolute from the Netflix series. The Robinsons takes place in the same universe as Bark Pack, and features a later generation of the Wilcox and Casey families, respectfully.