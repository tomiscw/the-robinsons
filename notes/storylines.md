# Storylines

## Proteus

The Robinson family in the movie stumbles upon the [Proteus](http://lostinspace.wikia.com/wiki/Proteus). They exist in an alternate timeline caused by a spatial anomaly created by Will's time machine on the planet they would crash on. The visual novel expands Proteus concept by making them a class of intersteller star ships that were placed throughout the path to Alpha Prime as a last ditch effort, due to the small window.

The time machine storyline, which is based on an actual episode, will be scrapped for a later time to focus on the Juipiter rescue mission and conflict with the SinoJordanians.

## SinoJordanians

The SinoJordanians is the original name given to the Sedition, as mentioned in the near final draft. They aimed to stop hypergate and succeeded with the loss Juipiter 2 on behalf of Smith, but did not account for the Proteus program in this visual novel.

Smith has been somewhat established to siding with the Sedition in the movie. This will be expanded upon as part of the Proteus arc.